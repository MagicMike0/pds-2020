#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: mike
"""

import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV


def staticTrainTestSplit(X, Y):
    return train_test_split(X, Y, test_size=0.30, random_state=0)

def predictRandomForestClassifier(X_train, X_test, Y_train):
    randFor = RandomForestClassifier(n_estimators=100, criterion='gini', max_depth=20, random_state=0)
    randFor.fit(X_train, Y_train)
    Y_predicted = randFor.predict(X_test)
    return Y_predicted

def predictLogisticRegression(X_train, X_test, Y_train):
    logreg = LogisticRegression(solver='lbfgs', max_iter=15000)
    logreg.fit(X_train, Y_train)
    Y_predicted = logreg.predict(X_test)
    return Y_predicted

def predictRandomForestGridSearchCV(X_train, X_test, Y_train):    
    parameters = {
        'n_estimators': np.arange(50, 201, 50),
        'max_depth': np.arange(10, 31, 5),
        'min_samples_split': np.arange(2, 16, 4),
        'min_samples_leaf': np.arange(2, 16, 4)
    }
    estimator = RandomForestClassifier(random_state=0)
    
    gs_model = GridSearchCV(estimator, parameters, verbose=1, cv=5, n_jobs=-1)
    gs_model.fit(X_train, Y_train)
    Y_predicted_gs = gs_model.predict(X_test)
    
    return Y_predicted_gs, gs_model.best_params_
