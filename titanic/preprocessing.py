#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: mike
"""

import pandas as pd
import numpy as np

""" Generic fonctions """

# take a dataframe
# return a dictionnary of columns with their proportion of missing data
def quantifyMissingData(df) :
    result = {}
    rowCount = df.shape[0]
    for col in df.columns :
        missingValueCount = df[col].isna().sum()
        if missingValueCount > 0 :
            result[col] = missingValueCount / rowCount
    return result

# take a dataframe
# return dataframe where missing data are replaced by the median for numbers and mode for objects
def fillMissingData(df):
    result = df.copy()
    for col in df.columns:
        if (df[col].dtype == np.dtype('float64')) or (df[col].dtype == np.dtype('int64')): # correction par rapport à la fonction envoyée - int64 != int && float64 != float
            result[col] = df[col].fillna(df[col].median())
        if (df[col].dtype == object):
            result[col] = df[col].fillna(df[col].mode()[0])
    return result

# take a dataframe
# return dataframe where missing data are either dropped (if 50% or more data are missing) or replaced by the median for numbers and mode for objects
def dropAndFillMissingData(df):
    result = df.copy()  
    #filter columns where 50% or more data are missing
    missingColumnProportions = quantifyMissingData(df)
    for col, value in missingColumnProportions.items() :
        if value > 0.5 :
            result = result.drop(columns=[col])
    return fillMissingData(result)

# take a dataframe, a target ('Survived' here), features (columns to keep in model) 
# return a target and a dataframe of either all columns or columns matching the given features
def splitDf(df, target, features = []) :
    y = df[target]
    if len(features) > 0 : x = df[features]
    else : x = df.drop([target], axis=1)
    return x, y

# take a dataframe, features (columns to dummify) 
# return a dataframe with the expected dummifyFeatures
def dummifyFeatures(df, features) :
    result = df.copy()
    for feature in features :
        dummies = pd.pandas.get_dummies(df[feature], prefix=feature, prefix_sep='_', dummy_na=False, columns=None, sparse=False, drop_first=False, dtype=None)
        result = result.drop([feature], axis=1)
        result = result.join(dummies, on=None, how='left', lsuffix='', rsuffix='', sort=False)
    return result

""" Specific fonctions """

# take a dataframe and transform the sex column to an integer
def computeSex(df) :
    df['IsMale'] = df['Sex'] == 'male'
    df['IsMale'] = df['IsMale'].astype(int)
    return df

# take a dataframe and transform the age column to 3 columns young, adult and old
def computeAgeClass(df) :
    children_mask = df['Age'] < 18
    not_old_mask = df['Age'] < 55
    df['IsChild'] = children_mask
    df['IsAdult'] = (children_mask != 1) & (not_old_mask)
    df['IsOld'] = not_old_mask != 1
    df["IsChild"] = df["IsChild"].astype(int)
    df["IsAdult"] = df["IsAdult"].astype(int)
    df["IsOld"] = df["IsOld"].astype(int)
    return df

# take a name and return the nested title if the title is frequent :
#'Mr': 517,
#'Mrs': 125,
#'Miss': 182,
#'Master': 40,
# otherwise return 'else'
def getTitle(str):
    frequentTitle = ['Mr', 'Mrs', 'Miss', 'Master']
    currentTitle = str.split(',')[1].split('.')[0].strip()
    if currentTitle in frequentTitle :
        return currentTitle
    else :
        return 'else'


# take a dataframe and transform the name column to title columns
def computeTitle(df):
    df['Title'] = df['Name'].apply(lambda x : getTitle(x))
    return df

# take a dataframe and drop irrelevant features
def dropUnusedFeatures(df):
    df = df.drop(['Name', 'Age', 'Sex', 'PassengerId', 'Embarked', 'Ticket'], axis=1)
    df = df.drop(['IsAdult', 'Title_else', 'Title_Miss', 'Pclass_2'], axis=1) #opt
    return df

def preprocessing(data):
    # clean column with more than 50% missing value and fill na
    cleanData = dropAndFillMissingData(data)
    # replace column age by IsChild, IsAduld and IsOld
    cleanData = computeAgeClass(cleanData)
    # replace column sex by IsMale
    cleanData = computeSex(cleanData)
    # replace column Name by Title
    cleanData = computeTitle(cleanData)
    # dumification of the 4 more common titles and all Pclass
    cleanData = dummifyFeatures(cleanData, ['Title', 'Pclass'])
    # drop irrelevant features left
    cleanData = dropUnusedFeatures(cleanData)
    # split the dataframe in a target and a set a features
    if ('Survived' in cleanData):
        return splitDf(cleanData, 'Survived')
    return cleanData, None